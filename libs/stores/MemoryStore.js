// ---------------------------------------- NATIVE MODULES ----------------------------------------

var util = require('util');

// ---------------------------------------- PUBLIC MODULES ----------------------------------------

var debug = require('debug')('memory'),
    async = require('async'),
    moment = require('moment');

// ---------------------------------------- CONSTRUCTOR ----------------------------------------

function MemoryStore(options) {
    this.cache = {};
    this.timeoutId = {};
    this.ttl = 0;
    this.overwrite = false;
    this.cacheSize = this._cacheSize();
    this.itemsCount = 0;
    this.maxCacheSize = 10485760;
    this.lru = [];

    if (options && typeof options === 'object' && !util.isArray(options)) {
        this.ttl = options.ttl || 0;
        this.overwrite = options.overwrite || false;
        this.maxCacheSize = options.maxCacheSize || 10485760;
    }

    if (this.maxCacheSize) this.maxCacheSize += this.cacheSize;

    debug(
        'INIT > instance with : ttl = %s ms, overwrite = %s, maxCacheSize = %s bytes',
        this.ttl,
        this.overwrite,
        this.maxCacheSize
    );

    debug(
        'STORE > cache = %s, timeoutId = %s, cacheSize = %s, itemsCount = %s',
        JSON.stringify(this.cache),
        JSON.stringify(this.timeoutId),
        this.cacheSize,
        this.itemsCount
    );
}

// ---------------------------------------- PUBLIC ----------------------------------------

MemoryStore.prototype.addItems = function (items, callback) {
    var self = this;

    if (typeof items === 'function' || !util.isArray(items)) return items(new Error('Undefined array'));
    if (typeof callback !== 'function') throw  new Error('Must be a function');

    async.eachSeries(
        items,
        function (item, callback) {
            if (!item.key || !item.value) return callback(new Error('Undefined key and/or value'));

            self._addItem(item, callback);
        },
        function (err) {
            if (err)return callback(err);

            callback();
        }
    );
};

MemoryStore.prototype.retrieveItems = function (keys, callback) {
    var self = this;

    if (typeof keys === 'function') return keys(new Error('Undefined key(s)'));
    if (typeof keys !== 'string' && !util.isArray(keys)) return callback(new Error('Must be a string or an array'));

    if (typeof keys === 'string') return callback(null, self._find(keys));

    var values = {};

    async.each(
        keys,
        function (key, callback) {
            var value = self._find(key);

            if (value) values[key] = value;

            callback();
        },
        function (err) {
            if (err) return callback(err);

            callback(null, values);
        }
    );
};

MemoryStore.prototype.removeItems = function (keys, callback) {
    var self = this;

    if (typeof keys === 'function') return keys(new Error('Undefined key(s)'));
    if (typeof keys !== 'string' && !util.isArray(keys)) return callback(new Error('Must be a string or an array'));

    if (typeof keys === 'string') return callback(null, self._delete(keys));

    async.each(
        keys,
        function (key, callback) {
            self._delete(key);
            callback();
        },
        function (err) {
            if (err) return callback(err);
            callback();
        }
    );
};

MemoryStore.prototype.clear = function (callback) {
    var self = this;

    var cacheKeys = Object.keys(self.cache);

    async.each(
        cacheKeys,
        function (key, callback) {
            self._delete(key);
            callback();
        },
        function (err) {
            if (err) return callback(err);
            callback();
        }
    );
};

MemoryStore.prototype.getItemsCount = function () {
    var self = this;

    return self.itemsCount;
};

MemoryStore.prototype.setTTL = function (ttl) {
    var self = this;

    if (typeof ttl === 'number') self.ttl = ttl;
};

MemoryStore.prototype.setOverwrite = function (overwrite) {
    var self = this;

    if (typeof overwrite === 'boolean') self.overwrite = overwrite;
};

MemoryStore.prototype.setMaxCacheSize = function (maxCacheSize) {
    var self = this;
    debug('STORE > set maxCacheSize to %s (old: %s) bytes', maxCacheSize, self.maxCacheSize);
    self.maxCacheSize = parseInt(maxCacheSize);
};

// ---------------------------------------- PRIVATE ----------------------------------------

MemoryStore.prototype._addItem = function (item, callback) {
    var self = this;

    item.size = self._itemSize(item.value);

    if (item.size > self.maxCacheSize) {
        debug(
            'DEBUG > "%s" item size (%s bytes) too big for actual cache size (%s bytes)',
            item.key,
            item.size,
            self.maxCacheSize
        );
        return callback();
    }

    debug(
        'ITEM > key = %s, value = %s, ttl = %s, overwrite = %s, size = %s',
        item.key,
        typeof item.value === 'object' ? JSON.stringify(item.value) : item.value,
        item.ttl,
        item.overwrite,
        item.size
    );

    if (item.overwrite == undefined || item.overwrite == null) item.overwrite = self.overwrite;

    if (self.cache[item.key]) {
        if (!item.overwrite) {
            debug('"%s" can not be overwritten with actual options', item.key);
            return callback();
        }

        if (item.size <= self._availableSize()) {
            return self._save(item, callback);
        }

        self._removeLeastRecentlyUsed(
            item.size,
            function () {
                self._save(item, callback);
            }
        );
    }

    if (item.size <= self._availableSize()) {
        return self._save(item, callback);
    }

    self._removeLeastRecentlyUsed(
        item.size,
        function () {
            self._save(item, callback);
        }
    );
};

MemoryStore.prototype._removeLeastRecentlyUsed = function (size, callback) {
    var self = this;

    var item = self.lru[0];

    self._delete(item.key);

    if (size <= self._availableSize()) return callback();

    self._removeLeastRecentlyUsed(size, callback);
};

MemoryStore.prototype._save = function (item, callback) {
    var self = this;

    debug('SAVE > item = %s', JSON.stringify(item));

    if (self.cache[item.key]) self._delete(item.key);

    self.cache[item.key] = item.value;

    self.cacheSize = self._cacheSize();
    self.itemsCount += 1;

    debug('SAVE > available cache size : %s', self._availableSize());

    self.lru.push({
        key: item.key,
        create_date: moment().unix(),
        access_date: 0,
        size: item.size
    });

    if (typeof item.ttl === 'number' && item.ttl == 0) return callback();

    if (!item.ttl && !self.ttl) return callback();

    if (!item.ttl) item.ttl = self.ttl;

    self.timeoutId[item.key] = setTimeout(
        function () {
            debug('TIMEOUT > key = %s', item.key);
            self._delete(item.key);
        },
        item.ttl
    );

    callback();
};

MemoryStore.prototype._find = function (key) {
    var self = this,
        data = self.cache[key];

    debug('FIND > key = %s, value = %s', key, typeof data === 'object' ? JSON.stringify(data) : data);

    if (!data) return;

    self._updateLRUItemAccessDate(key, moment().unix());

    return data;
};

MemoryStore.prototype._cacheSize = function () {
    var self = this;

    var size = self._size(self.cache);

    debug('CACHE > size : %s byte(s)', size);

    return size;
};

MemoryStore.prototype._itemSize = function (item) {
    var self = this;

    var size = self._size(item);

    debug('ITEM > size : %s byte(s)', size);

    return size;
};

MemoryStore.prototype._size = function (data) {
    var self = this;

    var string = typeof data === 'string' ? data : JSON.stringify(data);

    return Buffer.byteLength(string, 'utf8');
};

MemoryStore.prototype._availableSize = function () {
    var self = this;

    return self.maxCacheSize - self.cacheSize;
};

MemoryStore.prototype._delete = function (key) {
    var self = this;

    self._deleteLRUItem(key);

    if (self.timeoutId[key]) self._deleteTimeoutId(key);

    delete self.cache[key];

    self.cacheSize = self._cacheSize();
    self.itemsCount -= 1;

    debug('DELETE > key "%s"', key);
    debug('DELETE > available cache size : %s', self._availableSize());
};

MemoryStore.prototype._deleteTimeoutId = function (key) {
    var self = this;

    clearTimeout(self.timeoutId[key]);
    delete self.timeoutId[key];

    debug('DELETE > timeout for key "%s"', key);
};

MemoryStore.prototype._deleteLRUItem = function (key) {
    var self = this;

    for (var i = 0; i < self.lru.length; i++) {
        if (self.lru[i].key === key) {
            self.lru.splice(i, 1);
            break;
        }
    }

    debug('DELETE > "%s" remove from LRU Array', key);
    debug('LRU ARRAY > %s', JSON.stringify(self.lru));
};

MemoryStore.prototype._sortLRUArray = function () {
    var self = this;

    self.lru.sort(function (current, next) {
        return current.access_date - next.access_date;
    });

    debug('SORTED LRU ARRAY > %s', JSON.stringify(self.lru));
};

MemoryStore.prototype._updateLRUItemAccessDate = function (key, timestamp) {
    var self = this;

    for (var i = 0; i < self.lru.length; i++) {
        if (self.lru[i].key === key) {
            debug('UPDATE > "%s" access date from [%s] to [%s]', key, self.lru[i].access_date, timestamp);
            self.lru[i].access_date = timestamp;
            break;
        }
    }

    self._sortLRUArray();
};

// ---------------------------------------- EXPORT MODULE ----------------------------------------

module.exports = MemoryStore;