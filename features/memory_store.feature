Feature: Cache data using memory storage with default values

  Background:
    Given a cache configured with default values

  Scenario: add data in cache
    When I add data in cache
    Then I can retrieve this data
    And there is only 1 item in cache

  Scenario: add data in cache with ttl
    When I add data in cache with ttl
    Then I can retrieve this data
    And there is only 1 item in cache
    And data in cache has been removed after timeout

  Scenario: add data in cache which exceeds the maximum size
    Given a cache configured with a maximum cache size of 35410 bytes
    Given a cache with data
    When I add another data in cache
    Then old data has been removed and I can retrieve new data
    And there is only 1 item in cache