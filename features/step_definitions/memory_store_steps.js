module.exports = function () {
    this.Given(/^a cache configured with default values$/, function (callback) {
        this.prepareMemoryStore(callback);
    });

    this.When(/^I add data in cache$/, function (callback) {
        this.addDataInMemoryStore(callback);
    });

    this.Then(/^I can retrieve this data$/, function (callback) {
        this.assertDataInMemoryStore(callback);
    });

    this.Then(/^there is only (\d+) item in cache$/, function (count, callback) {
        this.assertOneItemInMemoryStore(count, callback);
    });

    this.When(/^I add data in cache with ttl$/, function (callback) {
        this.addDataInMemoryStoreWithTTL(callback);
    });

    this.Then(/^data in cache has been removed after timeout$/, function (callback) {
        this.assertDataHasBeenRemoved(callback);
    });

    this.Given(/^a cache configured with a maximum cache size of (\d+) bytes$/, function (size, callback) {
        this.prepareMemoryStoreAndSetMaxCacheSizeTo(size, callback);
    });

    this.Given(/^a cache with data$/, function (callback) {
        this.addDataInMemoryStore(callback);
    });

    this.When(/^I add another data in cache$/, function (callback) {
        this.addAnotherDataInMemoryStore(callback);
    });

    this.Then(/^old data has been removed and I can retrieve new data$/, function (callback) {
        this.assertOldDataHasBeenRemovedAndNewDataInMemoryStore(callback);
    });
};