var fs = require('fs'),
    path = require('path'),
    attempt = require('attempt'),
    Faker = require('Faker'),
    async = require('async');
var keepsake = require('../../libs/index');

module.exports = function () {
    this.World = KeepsakeWorld;

    function KeepsakeWorld(callback) {
        callback();
    }

    KeepsakeWorld.prototype.prepareMemoryStore = function (callback) {
        var self = this;

        self.memoryStore = new keepsake.MemoryStore();

        callback();
    };

    KeepsakeWorld.prototype.prepareMemoryStoreAndSetMaxCacheSizeTo = function (size, callback) {
        var self = this;
        self.memoryStore.setMaxCacheSize(size);
        callback();
    };

    KeepsakeWorld.prototype.addDataInMemoryStore = function (callback) {
        var self = this;

        if (!self.memoryStore instanceof keepsake.MemoryStore) return callback(new Error('Storage isn\'t a instance of MemoryStore'));

        self.key = 'list';
        self.value = self._generateDataJSON();

        var items = [
            {
                key: self.key,
                value: self.value
            }
        ];

        self.memoryStore.addItems(items, callback);
    };

    KeepsakeWorld.prototype.addDataInMemoryStoreWithTTL = function (callback) {
        var self = this;

        self.key = 'list';
        self.value = self._generateDataJSON();

        var items = [
            {
                key: self.key,
                value: self.value,
                ttl: 5000
            }
        ];

        self.memoryStore.addItems(items, callback);
    };

    KeepsakeWorld.prototype.addAnotherDataInMemoryStore = function (callback) {
        var self = this;

        if (!self.memoryStore instanceof keepsake.MemoryStore) return callback(new Error('Storage isn\'t a instance of MemoryStore'));

        self.newKey = 'myObject';
        self.value = {
            a: Faker.Lorem.words(20).join(' ')
        };

        var items = [
            {
                key: self.newKey,
                value: self.value
            }
        ];

        self.memoryStore.addItems(items, callback);
    };

    KeepsakeWorld.prototype.assertDataInMemoryStore = function (callback) {
        var self = this;

        attempt(
            {
                retries: 10,
                interval: 5,
                factor: 2
            },
            function () {
                var callback = this;
                self.memoryStore.retrieveItems(self.key, callback);
            },
            function (err, value) {
                if (err) return callback(err);
                if (!value) return callback(new Error('Data not found in cache'));
                callback();
            }
        );
    };

    KeepsakeWorld.prototype.assertOneItemInMemoryStore = function (count, callback) {
        var self = this;

        var itemsCount = self.memoryStore.getItemsCount();

        if (itemsCount == count)
            callback();
        else
            callback(new Error('Must be only ' + count + ' item, actually there is : ' + itemsCount));
    };

    KeepsakeWorld.prototype.assertDataHasBeenRemoved = function (callback) {
        var self = this;

        attempt(
            {
                retries: 10,
                interval: 5,
                factor: 2
            },
            function () {
                var callback = this;
                self.memoryStore.retrieveItems(self.key, function (err, value) {
                    if (err) return callback(err);
                    if (value) return callback(new Error('Data still exists in cache'));
                    callback();
                });
            },
            callback
        );
    };

    KeepsakeWorld.prototype.assertOldDataHasBeenRemovedAndNewDataInMemoryStore = function (callback) {
        var self = this;

        async.series(
            {
                oldData: function (callback) {
                    attempt(
                        {
                            retries: 10,
                            interval: 5,
                            factor: 2
                        },
                        function () {
                            var callback = this;
                            self.memoryStore.retrieveItems(self.key, function (err, value) {
                                if (err) return callback(err);
                                if (value) return callback(new Error('Data still exists in cache'));
                                callback();
                            });
                        },
                        callback
                    );
                },
                newData: function (callback) {
                    attempt(
                        {
                            retries: 10,
                            interval: 5,
                            factor: 2
                        },
                        function () {
                            var callback = this;
                            self.memoryStore.retrieveItems(self.newKey, callback);
                        },
                        function (err, value) {
                            if (err) return callback(err);
                            if (!value) return callback(new Error('Data not found in cache'));
                            callback();
                        }
                    );
                }
            },
            function (err, results) {
                callback(err);
            }
        );
    };

    KeepsakeWorld.prototype._generateDataJSON = function () {
        var string = fs.readFileSync(path.join(__dirname, '..', 'fixtures', 'data.json'));
        return JSON.parse(string);
    };
};