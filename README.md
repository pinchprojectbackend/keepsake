# KEEPSAKE

Keepsake of items infinitely or forget them after a number of times.

## Example

See also the examples folder.

## Installation

```
npm install keepsake
```

## Usage

Include the module

```javascript
var keepsake = require('keepsake');
```

### Possible storages

* **MemoryStore** for using memory storage

**NB!** For now you can only use the **MemoryStore**.

### Setting up MemoryStore

Possible store options are the following :

* **ttl** - define a global TTL in milliseconds for each items added, after that time the key will be removed (default to 0 means **infinity**)
* **overwrite** - set if you want to update value for a key that already exists (default to **false**)
* **maxCacheSize** - set the store size in bytes (default to **10485760 Bytes = 10 MB**)

Example :

```javascript
var MemoryStore = require('keepsake').MemoryStore;

// with custom options
var customCache = new MemoryStore({
	ttl: 1000,
	overwrite: true,
	maxCacheSize: 1024
});

// with defaults values
var defaultCache = new MemoryStore();
```

After initialization, you can update **TTL**, **overwrite** and **maxCacheSize** attributes with these methods :

* **setTTL** - define new global time-to-live (in **ms**)
* **setOverwrite** - set if store can update or not the key value
* **setMaxCacheSize** - define new cache size (in **bytes**)

Example :

```javascript
defaultCache.setTTL(5000); // set TTL to 5 seconds
defaultCache.setOverwrite(true); // key can be update
defaultCache.setMaxCacheSize(2048); // set max cache size to 2MB
```

### Available methods

#### Add item(s) in store

Possible parameters :

* **Array** - an array of object with 2 attributes : **key** and **value**
* **Function** - callback function

Example :

```javascript
// add one item
defaultCache.addItems(
	[
		{ "key": "one", "value": "oneValue" }
	],
	function (err) {
		if (err) return console.log(err);
		console.log('Item(s) added');
	}
);

// add more than one item
defaultCache.addItems(
	[
		{ "key": "one", "value": "oneValue" },
		{ "key": "two", "value": "twoValue" },
		{ "key": "three", "value": "threeValue" },
		{ "key": "four", "value": "fourValue" }
	],
	function (err) {
		if (err) return console.log(err);
		console.log('Item(s) added');
	}
);
```

**NB!** If the size of an element is greater than the available space, the least recently used items will be removed until there is enough space for the new element to add.

#### Retrieve item(s) from store

Possible parameters :

* **Array, String** - can be a key or an array of keys
* **Function** - callback function

Example :

```javascript
// retrieve value from one item
defaultCache.retrieveItems(
	"one",
	function (err, data) {
		if (err) return console.log(err);
		if (!data) return console.log('Item(s) not found');
		console.log(data);
	}
);

// retrieve values from multiple items
defaultCache.retrieveItems(
	[ "one", "two", "three", "four" ],
	function (err, data) {
		if (err) return console.log(err);
		if (!data) return console.log('Item(s) not found');
		console.log(data);
	}
);
```

#### Remove item(s) from store

Possible parameters :

* **Array, String** - can be a key or an array of keys
* **Function** - callback function

Example :

```javascript
// retrieve value from one item
defaultCache.removeItems(
	"one",
	function (err) {
		if (err) return console.log(err);
		console.log('Item(s) removed');
	}
);

// retrieve values from multiple items
defaultCache.removeItems(
	[ "one", "two", "three", "four" ],
	function (err, data) {
		if (err) return console.log(err);
		console.log('Item(s) removed');
	}
);
```

#### Clear store

Possible parameter :

* **Function** - callback function

Example :

```javascript
defaultCache.clear(function (err) {
	if (err) return console.log(err);
	console.log('Storage cleared');
});
```

#### Retrieve the number of items in store

Example :

```javascript
console.log(defaultCache.getItemsCount()); // display the number of items in memory store
```

## Tests

Run the tests with npm in keepsake directory

```
npm test
```

## License

**keepsake** is licensed under MIT License.